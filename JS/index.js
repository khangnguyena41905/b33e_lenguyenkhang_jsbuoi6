function handle_1() {
  var sum = 0;
  var m = 0;
  for (var n = 0; sum < 10000; n++) {
    sum += n;
    m = n;
  }
  document.getElementById(
    "result1"
  ).innerHTML = `<h1> Số nguyên nhỏ nhất là ${m} </h1>`;
}

function handle_2() {
  const max = document.getElementById("txt-max").value * 1;
  const x = document.getElementById("txt-x").value * 1;
  var sum = 0;
  for (var n = 1; n <= max; n++) {
    sum += Math.pow(x, n);
  }
  document.getElementById("result2").innerHTML = `<h1> Kết quả là ${sum} </h1>`;
}

function handle_3() {
  var sum = 1;
  const n = document.getElementById("txt-n").value * 1;
  for (var i = 1; i <= n; i++) {
    sum *= i;
  }
  document.getElementById(
    "result3"
  ).innerHTML = `<h1> Giai thừa là ${sum} </h1>`;
}

function handle_4() {
  var contentEl = "";
  for (var i = 1; i <= 10; i++) {
    if (i % 2 != 0) {
      contentEl += `<h1 class="bg-primary"> Div lẻ ${i} </h1>`;
    } else {
      contentEl += `<h1 class="bg-danger"> Div chẵn ${i} </h1>`;
    }
  }
  document.getElementById("result4").innerHTML = contentEl;
}
